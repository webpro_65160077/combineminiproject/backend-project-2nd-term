import { IsNotEmpty } from 'class-validator';

export class CreateBranchDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  maneger: string;

  @IsNotEmpty()
  address: string;

  @IsNotEmpty()
  tels: string;

  stock: string;
}
