import { Stock } from 'src/stocks/entities/stock.entity';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Branch {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  maneger: string;

  @Column()
  address: string;

  @Column()
  tels: string;

  @OneToMany(() => Stock, (stock) => stock.branch)
  stock: Stock;
}
