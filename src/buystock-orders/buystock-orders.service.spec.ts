import { Test, TestingModule } from '@nestjs/testing';
import { BuystockOrdersService } from './buystock-orders.service';

describe('BuystockOrdersService', () => {
  let service: BuystockOrdersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BuystockOrdersService],
    }).compile();

    service = module.get<BuystockOrdersService>(BuystockOrdersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
