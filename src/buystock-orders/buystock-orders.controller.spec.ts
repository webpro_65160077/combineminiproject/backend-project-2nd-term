import { Test, TestingModule } from '@nestjs/testing';
import { BuystockOrdersController } from './buystock-orders.controller';
import { BuystockOrdersService } from './buystock-orders.service';

describe('BuystockOrdersController', () => {
  let controller: BuystockOrdersController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BuystockOrdersController],
      providers: [BuystockOrdersService],
    }).compile();

    controller = module.get<BuystockOrdersController>(BuystockOrdersController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
