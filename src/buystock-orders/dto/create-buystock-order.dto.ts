export class CreateBuystockOrderDto {
  buystockOrderItems: {
    productId: number;
    qty: number;
  }[];
  userId: number;
}
