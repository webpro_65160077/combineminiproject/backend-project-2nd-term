import { PartialType } from '@nestjs/swagger';
import { CreateBuystockOrderDto } from './create-buystock-order.dto';

export class UpdateBuystockOrderDto extends PartialType(
  CreateBuystockOrderDto,
) {}
