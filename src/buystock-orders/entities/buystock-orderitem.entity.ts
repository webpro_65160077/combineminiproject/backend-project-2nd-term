import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Buystockorder } from './buystock-order.entity';
import { BuyStock } from 'src/buystocks/entities/buyStock.entity';

@Entity()
export class BuystockorderItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column()
  price: number;

  @Column()
  qty: number;

  @Column()
  total: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToOne(
    () => Buystockorder,
    (buystockorders) => buystockorders.buystockorderItems,
    {
      onDelete: 'CASCADE',
    },
  )
  buystockorders: Buystockorder;

  @ManyToOne(() => BuyStock, (buyStock) => buyStock.buystockorderItems)
  buystock: BuyStock;
}
