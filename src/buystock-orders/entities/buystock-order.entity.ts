import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { BuystockorderItem } from './buystock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';

@Entity()
export class Buystockorder {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  total: number;

  @Column()
  qty: number;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @OneToMany(
    () => BuystockorderItem,
    (buystockorderItems) => buystockorderItems.buystockorders,
  )
  buystockorderItems: BuystockorderItem[];

  @ManyToOne(() => User, (user) => user.buystockorders)
  user: User;
}
