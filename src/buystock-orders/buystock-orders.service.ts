import { Injectable } from '@nestjs/common';
import { CreateBuystockOrderDto } from './dto/create-buystock-order.dto';
import { UpdateBuystockOrderDto } from './dto/update-buystock-order.dto';
import { Buystockorder } from './entities/buystock-order.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { BuystockorderItem } from './entities/buystock-orderitem.entity';
import { User } from 'src/users/entities/user.entity';
import { BuyStock } from 'src/buystocks/entities/buyStock.entity';

@Injectable()
export class BuystockOrdersService {
  constructor(
    @InjectRepository(Buystockorder)
    private buystockordersRepository: Repository<Buystockorder>,
    @InjectRepository(BuystockorderItem)
    private buystockorderItemsRepository: Repository<BuystockorderItem>,
    @InjectRepository(User) private usersRepository: Repository<User>,
    @InjectRepository(BuyStock)
    private buystocksRepository: Repository<BuyStock>,
  ) {}
  async create(createBuystockOrderDto: CreateBuystockOrderDto) {
    const buystockOrder = new Buystockorder();
    const user = await this.usersRepository.findOneBy({
      id: createBuystockOrderDto.userId,
    });
    buystockOrder.user = user;
    buystockOrder.total = 0;
    buystockOrder.qty = 0;
    buystockOrder.buystockorderItems = [];
    for (const oi of createBuystockOrderDto.buystockOrderItems) {
      const buystockOrderItem = new BuystockorderItem();
      buystockOrderItem.buystock = await this.buystocksRepository.findOneBy({
        id: oi.productId,
      });
      buystockOrderItem.name = buystockOrderItem.buystock.name;
      buystockOrderItem.price = buystockOrderItem.buystock.price;
      buystockOrderItem.qty = oi.qty;
      buystockOrderItem.total = buystockOrderItem.price * buystockOrderItem.qty;
      await this.buystockorderItemsRepository.save(buystockOrderItem);
      buystockOrder.buystockorderItems.push(buystockOrderItem);
      buystockOrder.total += buystockOrderItem.total;
      buystockOrder.qty += buystockOrderItem.qty;
    }
    return this.buystockordersRepository.save(buystockOrder);
  }

  findAll() {
    return this.buystockordersRepository.find({
      relations: { buystockorderItems: true },
      order: {
        id: 'desc',
      },
    });
  }

  findOne(id: number) {
    return this.buystockordersRepository.findOneOrFail({
      where: { id },
      relations: { buystockorderItems: true },
    });
  }

  update(id: number, UpdateBuystockOrderDto: UpdateBuystockOrderDto) {
    return `This action updates a #${id} order`;
  }

  async remove(id: number) {
    const deleteOrder = await this.buystockordersRepository.findOneOrFail({
      where: { id },
    });
    await this.buystockordersRepository.remove(deleteOrder);

    return deleteOrder;
  }
}
