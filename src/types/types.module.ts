import { Module } from '@nestjs/common';
import { TypesService } from './types.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TypesController } from './types.controller';
import { Type } from './entities/type.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Type])],
  controllers: [TypesController],
  providers: [TypesService],
})
export class TypesModule {}
