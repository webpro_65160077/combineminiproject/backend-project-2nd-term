import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Promotion } from './promotion.entity';

@Entity()
export class PromotionItem {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: true })
  discountPercent: number | null;

  @Column({ nullable: true })
  discountPrice: number | null;

  @Column({ nullable: true })
  conditionQty: number | null;

  @Column({ nullable: true })
  conditionPrice: number | null;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(() => Promotion, (promotion) => promotion.promotionItems, {
    onDelete: 'CASCADE',
  })
  promotions: Promotion[];

  @Column({ nullable: true })
  productId?: number;
}
