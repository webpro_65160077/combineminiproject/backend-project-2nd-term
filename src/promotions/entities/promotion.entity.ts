import {
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { PromotionItem } from './promotionItem.entity';

@Entity()
export class Promotion {
  @PrimaryGeneratedColumn()
  id?: number;

  @Column()
  code: string;

  @Column()
  name: string;

  @Column({ type: 'date' })
  startdate: string;

  @Column({ type: 'date' })
  enddate: string;

  @CreateDateColumn()
  created: Date;

  @UpdateDateColumn()
  updated: Date;

  @ManyToMany(
    () => PromotionItem,
    (promotionItem) => promotionItem.promotions,
    {
      cascade: true,
    },
  )
  @JoinTable()
  promotionItems?: PromotionItem[];
}
