import { Injectable } from '@nestjs/common';
import { CreatePromotionDto } from './dto/create-promotion.dto';
import { UpdatePromotionDto } from './dto/update-promotion.dto';
import { Promotion } from './entities/promotion.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class PromotionsService {
  constructor(
    @InjectRepository(Promotion)
    private promotionsRepository: Repository<Promotion>,
  ) {}
  create(createPromotionDto: CreatePromotionDto) {
    return this.promotionsRepository.save(createPromotionDto);
  }

  findAll() {
    return this.promotionsRepository.find({
      relations: { promotionItems: true },
    });
  }

  findOne(id: number) {
    // วิธีหาตัวเดียวและเอาความสัมพันธ์มาด้วย
    return this.promotionsRepository.findOne({
      where: { id },
      relations: { promotionItems: true },
    });
  }

  async update(id: number, updatePromotionDto: UpdatePromotionDto) {
    // ทำไม่ได้ใช้post เลย
    // ถ้าอยากเคลียร์ เรื่อง PromotionItem id ค่อยกลับมาเคลียร์
    // หมายเหตุตอนนี้ถ้า ใช้ postman ยิง แล้ว promoiton Id เดียว กัน มันจะแก้ให้ที่เดียว
    return await this.promotionsRepository.save(updatePromotionDto);
  }

  async remove(id: number) {
    const deletePromotion = await this.promotionsRepository.findOneBy({ id });
    return this.promotionsRepository.remove(deletePromotion);
  }
}
