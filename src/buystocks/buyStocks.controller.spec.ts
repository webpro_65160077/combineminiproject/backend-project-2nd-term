import { IsNotEmpty } from 'class-validator';

export class CreateBuyStockDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  balance: number;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  cost: number;
}
