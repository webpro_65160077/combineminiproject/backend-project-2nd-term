import { Injectable } from '@nestjs/common';
import { CreateBuyStockDto } from './dto/create-buyStock.dto';
import { UpdateBuyStockDto } from './dto/update-buyStock.dto';
import { BuyStock } from './entities/buyStock.entity';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class BuyStocksService {
  constructor(
    @InjectRepository(BuyStock)
    private buyStocksRepository: Repository<BuyStock>,
  ) {}
  create(createBuyStockDto: CreateBuyStockDto) {
    const buyStock = new BuyStock();
    buyStock.name = createBuyStockDto.name;
    buyStock.price = parseFloat(createBuyStockDto.price);
    buyStock.unit = createBuyStockDto.unit;
    return this.buyStocksRepository.save(buyStock);
  }

  findAll() {
    return this.buyStocksRepository.find();
  }

  findOne(id: number) {
    return this.buyStocksRepository.findOne({
      where: { id },
    });
  }

  async update(id: number, updateBuyStockDto: UpdateBuyStockDto) {
    const buyStock = await this.buyStocksRepository.findOneOrFail({
      where: { id },
    });
    buyStock.name = updateBuyStockDto.name;
    buyStock.price = parseFloat(updateBuyStockDto.price);
    buyStock.unit = updateBuyStockDto.unit;
    this.buyStocksRepository.save(buyStock);
    const result = await this.buyStocksRepository.findOne({
      where: { id },
    });
    return result;
  }

  async remove(id: number) {
    const deleteBuyStock = await this.buyStocksRepository.findOneOrFail({
      where: { id },
    });
    await this.buyStocksRepository.remove(deleteBuyStock);

    return deleteBuyStock;
  }
}
