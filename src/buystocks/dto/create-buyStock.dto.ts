export class CreateBuyStockDto {
  name: string;
  price: string;
  unit: string;
}
