import { Module } from '@nestjs/common';
import { BuyStocksService } from './buyStocks.service';
import { BuyStocksController } from './buyStocks.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { BuyStock } from './entities/buyStock.entity';

@Module({
  imports: [TypeOrmModule.forFeature([BuyStock])],
  controllers: [BuyStocksController],
  providers: [BuyStocksService],
})
export class BuyStocksModule {}
