import { Test, TestingModule } from '@nestjs/testing';
import { BuyStocksService } from './buyStocks.service';

describe('BuyStocksService', () => {
  let service: BuyStocksService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [BuyStocksService],
    }).compile();

    service = module.get<BuyStocksService>(BuyStocksService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
