import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TemperatureModule } from './temperature/temperature.module';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from './users/entities/user.entity';
import { DataSource } from 'typeorm';
import { MembersModule } from './members/members.module';
import { Member } from './members/entities/member.entity';
import { Branch } from './branch/entities/branch.entity';
import { BranchModule } from './branch/branch.module';
import { PromotionsModule } from './promotions/promotions.module';
import { Promotion } from './promotions/entities/promotion.entity';
import { PromotionItem } from './promotions/entities/promotionItem.entity';
import { Stock } from './stocks/entities/stock.entity';
import { StocksModule } from './stocks/stocks.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { Role } from './roles/entities/role.entity';
import { Type } from './types/entities/type.entity';
import { Product } from './products/entities/product.entity';
import { OrderItem } from './orders/entities/orderItem.entity';
import { Order } from './orders/entities/order.entity';
import { ProductsModule } from './products/products.module';
import { RolesModule } from './roles/roles.module';
import { TypesModule } from './types/types.module';
import { AuthModule } from './auth/auth.module';
import { OrdersModule } from './orders/orders.module';

import { BuyStocksModule } from './buystocks/buyStocks.module';
import { BuystockOrdersModule } from './buystock-orders/buystock-orders.module';
import { Buystockorder } from './buystock-orders/entities/buystock-order.entity';
import { BuyStock } from './buystocks/entities/buyStock.entity';
import { BuystockorderItem } from './buystock-orders/entities/buystock-orderitem.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [
        User,
        Role,
        OrderItem,
        Type,
        Product,
        Branch,
        Stock,
        BuyStock,
        Buystockorder,
        BuystockorderItem,
        Member,
        Order,
        Promotion,
        PromotionItem,
      ],
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
    }),
    RolesModule,
    TypesModule,
    ProductsModule,
    TemperatureModule,
    UsersModule,
    MembersModule,
    BranchModule,
    StocksModule,
    PromotionsModule,
    BuyStocksModule,
    OrdersModule,
    AuthModule,
    BuystockOrdersModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) {}
}
