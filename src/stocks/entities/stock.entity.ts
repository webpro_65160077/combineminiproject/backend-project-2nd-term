import { Branch } from 'src/branch/entities/branch.entity';
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Stock {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ default: 'noimage.jpg' })
  image: string;

  @Column()
  min: number;

  @Column()
  balance: number;

  @Column()
  unit: string;

  @Column({
    default: 'Available',
  })
  status: string;

  @ManyToOne(() => Branch, (branch) => branch.stock)
  branch: Branch;
}
