import { IsNotEmpty } from 'class-validator';

export class CreateStockDto {
  @IsNotEmpty()
  name: string;
  @IsNotEmpty()
  min: string;
  @IsNotEmpty()
  balance: string;
  @IsNotEmpty()
  unit: string;
  @IsNotEmpty()
  status: string;

  image: string;

  branch: string;
}
